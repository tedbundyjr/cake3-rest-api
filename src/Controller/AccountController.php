<?php

namespace App\Controller;

use Exception;
use RestApi\Controller\ApiController;
use RestApi\Utility\JwtToken;

/**
 * Account Controller
 *
 */
class AccountController extends ApiController
{

    /**
     * Login method
     *
     * @return void
     */
    public function login()
    {
        $this->request->allowMethod('post');
        $this->loadModel('Users');
        $entity = $this->Users->newEntity($this->request->data, ['validate' => 'LoginApi']);
        if ($entity->errors()) {
            $this->httpStatusCode = 400;
            $this->apiResponse['message'] = 'Validation failed.';
            foreach ($entity->errors() as $field => $validationMessage) {
                $this->apiResponse['error'][$field] = $validationMessage[key($validationMessage)];
            }
        } else {
            $user = $this->Users->find()
                ->where([
                    'email' => $entity->email,
                    'password' => md5($entity->password),
                    'status' => 1,
                ])
                ->first();
            if (empty($user)) {
                $this->httpStatusCode = 403;
                $this->apiResponse['error'] = 'Invalid email or password.';
                return;
            }
            $payload = ['email' => $user->email, 'name' => $user->name];
            $this->apiResponse['token'] = JwtToken::generateToken($payload);
            $this->apiResponse['message'] = 'Logged in successfully.';
            unset($user);
            unset($payload);
        }
    }

    /**
     * Register method
     *
     * Returns a token on successful registration
     *
     * @return void
     */
    public function register()
    {
        $this->request->allowMethod('post');

        $this->loadModel('Users');

        $user = $this->Users->newEntity($this->request->data());
        try {
            if ($this->Users->save($user)) {
                $this->apiResponse['message'] = 'Registered successfully.';
                $payload = ['email' => $user->email, 'name' => $user->name];
                $this->apiResponse['token'] = JwtToken::generateToken($payload);
            } else {
                $this->httpStatusCode = 400;
                $this->apiResponse['message'] = 'Unable to register user.';
                if ($user->errors()) {
                    $this->apiResponse['message'] = 'Validation failed.';
                    foreach ($user->errors() as $field => $validationMessage) {
                        $this->apiResponse['error'][$field] = $validationMessage[key($validationMessage)];
                    }
                }
            }
        } catch (Exception $e) {
            $this->httpStatusCode = 400;
            $this->apiResponse['message'] = 'Unable to register user.';
        }

        unset($user);
        unset($payload);
    }
}
