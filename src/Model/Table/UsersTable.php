<?php
 
namespace App\Model\Table;
 
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
 
/**
 * Users Model
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
 
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
 
        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');
 
        $this->addBehavior('Timestamp');
    }
 
    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');
 
        $validator
            ->requirePresence('name', 'create', 'This is required parameter.')
            ->notEmpty('name', 'Name is required.');
 
        $validator
            ->email('email', 'Please provide a valid email address')
            ->requirePresence('email', 'create', 'This is required parameter.')
            ->notEmpty('email', 'Email address is required.');
 
        $validator
            ->requirePresence('password', 'create', 'This is required parameter.')
            ->notEmpty('password', 'Password is required.');
 
        $validator
            ->boolean('status')
            ->allowEmpty('status', 'create');
 
        return $validator;
    }
 
    /**
     * Default validation rules.
     *
     * @param Validator $validator Validator instance.
     * @return Validator
     */
    public function validationLoginApi(Validator $validator)
    {
        $validator
            ->email('email', 'Please provide a valid email address.')
            ->requirePresence('email', 'create', 'This is required parameter.')
            ->notEmpty('email', 'Email address is required');
 
        $validator
            ->requirePresence('password', 'create', 'This is required parameter.')
            ->notEmpty('password', 'Password is required.');
 
        return $validator;
    }
 
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param RulesChecker $rules The rules object to be modified.
     * @return RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], 'The email address is already in use. Please use a different email address.'));
 
        return $rules;
    }
 
    /**
     * Modifies password before saving into database
     *
     * @param Event $event Event
     * @param EntityInterface $entity Entity
     * @param ArrayObject $options Array of options
     * @return bool
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (isset($entity->password)) {
            $entity->password = md5($entity->password);
        }
 
        return true;
    }
}